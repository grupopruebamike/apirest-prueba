using System;
using Xunit;
using CursoPlatzi.Controllers;
//
namespace Pruebas
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            ValuesController valuesController = new ValuesController();
            string valor = "Test1";
            string resultado = valuesController.Get(valor).Value;
            Assert.Equal(valor, resultado);
        }
    }
}